# Layout renderer demo app
Angular2 demo application that uses the abs-layout-renderer to display templates & controls dynamically.

## Prerequisites
* Be sure that the abs-layout renderer is built before you run this demo
* Control store must be running

## Run
```
npm install -g jspm
npm install
jspm link ../abs-layout-renderer/dist # relative path to the built abs-layout-renderer library
jspm install
npm run serve
```
