import { Component, ComponentFactoryResolver, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { LayoutRendererComponent } from 'abs-layout-renderer';

declare var SystemJS;
declare var Reflect;

@Component({
  selector: 'root-app',
  moduleId: __moduleName,
  templateUrl: 'hello.html'
})
export class HelloComponent {
  public moduleStore = 'http://localhost:3002/';
  public templateStore = 'http://localhost:3002/templates/';
  public templateId = 'my-template-1.html';
  public dossierNr: string;
  public dossiers = [];

  @ViewChild('lrContainer', { read: ViewContainerRef }) lrContainer: ViewContainerRef;

  constructor(private cmpFactoryResolver: ComponentFactoryResolver) {}

  public loadTemplate() {
    const cmpFactory = this.cmpFactoryResolver.resolveComponentFactory(LayoutRendererComponent);
    this.lrContainer.clear();
    const cmpRef = this.lrContainer.createComponent(cmpFactory);
    cmpRef.instance.moduleStore = this.moduleStore;
    cmpRef.instance.templateStore = this.templateStore;
    cmpRef.instance.templateId = this.templateId;
    cmpRef.instance.context = { dossierNr: this.dossierNr };
    cmpRef.instance.onEmitted.subscribe((event) => {
      this.dossiers.push(event);
      this.lrContainer.clear();
    });
    this.dossierNr = '';
  }

}
