import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AbsLayoutRendererModule } from 'abs-layout-renderer';

import { routing, RootComponent } from './routes.ts';
import { HelloComponent } from './hello.ts';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    AbsLayoutRendererModule.forRoot()
  ],
  declarations: [
    RootComponent,
    HelloComponent
  ],
  bootstrap: [RootComponent]
})
export class AppModule { }
